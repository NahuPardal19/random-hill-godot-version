extends Node2D

##VARIABLES MODIFICABLES
var can_fire = true
@onready var bullet_direction = 1

@export var cooldown_time:float

##Nodos necesitados
@onready var cooldown = $Cooldown
@onready var barrel_position = $BarrelPosition
@onready var sprite = $Sprite
var bullet = preload("res://Scenes/Bullets/Normal Bullet.tscn")
var player : Player

func _ready():
	cooldown.wait_time = cooldown_time
	set_as_top_level(true)
	player = get_parent()
func _process(delta):
	FollowPlayer()
	Shoot()

func FollowPlayer():
		position.x = lerp(position.x , get_parent().position.x , 0.5)
		position.y = lerp(position.y , get_parent().position.y , 0.5)
func Shoot():
#region Aim
		#MOUSE
		#if player.Player_Number == Player.Player_Number_Enum.PLAYER_1:
			#var mouse_position = get_global_mouse_position()
			#look_at(mouse_position)
		#
		##CONTROL
		#else:
			#var look_vector = Vector2()
			#look_vector.x = Input.get_action_strength(player.stringPlayerInput+"AimRight") - Input.get_action_strength(player.stringPlayerInput+"AimLeft")
			#look_vector.y = Input.get_action_strength(player.stringPlayerInput+"AimDown") - Input.get_action_strength(player.stringPlayerInput+"AimUp")
			#look_at(position + look_vector)
		
	
#endregion
#region Shoot
		
		if cooldown.time_left > 0: can_fire = false
		else: can_fire = true

		if Input.is_action_just_pressed(player.stringPlayerInput +"Shoot") and can_fire:
			
			#Instanciacion de la bala e impulsando
			var bullet_instance =  bullet.instantiate()
			bullet_instance.player = player
			bullet_instance.global_position = $BarrelPosition.global_position
			bullet_instance.rotation_degrees = rotation_degrees
			bullet_instance.apply_impulse(Vector2(GAME_MANAGER.NormalGun_ForceShoot * bullet_direction,0).rotated($BarrelPosition.rotation), $BarrelPosition.position)
			get_tree().get_root().add_child(bullet_instance)
			cooldown.start()
func RotateGun(fliph:bool):
	if !fliph:
		sprite.flip_h = fliph
		barrel_position.position.x = 6.0
		bullet_direction = 1
	else:
		sprite.flip_h = fliph
		barrel_position.position.x = -6.0
		bullet_direction = -1
#endregion
