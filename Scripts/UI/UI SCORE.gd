extends Control

@onready var score_player_1 = $VBoxContainer/ScorePlayer1
@onready var score_player_2 = $VBoxContainer/ScorePlayer2
@onready var score_player_3 = $VBoxContainer/ScorePlayer3
@onready var score_player_4 = $VBoxContainer/ScorePlayer4

@export var label_timer : Label
@export var label_events : Label

func _ready():
	#Obtengo las Label de los score de todos los jugadores (a partir de su containers)
	UI_MANAGER._ScorePlayer1Label = score_player_1
	UI_MANAGER._ScorePlayer2Label = score_player_2
	UI_MANAGER._ScorePlayer3Label = score_player_3
	UI_MANAGER._ScorePlayer4Label = score_player_4
	
	UI_MANAGER._TimerLabel = label_timer 
	UI_MANAGER._EventLabel = label_events 


func _on_replay_pressed():
	get_tree().change_scene_to_file("res://Scenes/Maps/Test map.tscn")


func _on_menu_pressed():
	get_tree().change_scene_to_file("res://Scenes/Maps/Menu.tscn")


