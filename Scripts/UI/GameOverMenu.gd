extends TextureRect


func _ready():
	UI_MANAGER._GameOverUI = $"."
	GAME_MANAGER.FinalPartida.connect(_PutPlayerWinner)

func _PutPlayerWinner(winner:Player):
	$"VBoxContainer/PLAYER WINS".text = winner.get_name() + "WINS"
