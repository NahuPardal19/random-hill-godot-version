extends RigidBody2D

@onready var Sprite = $Sprite


@export var Speed : float
@export var Jump_Force : float

##RAYCASTS
@onready var ray_cast_left = $Raycasts/RayCastLeft
@onready var ray_cast_right = $Raycasts/RayCastRight


func _ready():
	pass # Replace with function body.


func _integrate_forces(state):
	_Move()
	_Jump()


func _Move():
	var direction = Input.get_axis("Left", "Right")
	
	if direction:
		linear_velocity = Vector2(direction*Speed,linear_velocity.y)
	else: linear_velocity = Vector2(0,linear_velocity.y)
	
	if direction > 0:
		Sprite.flip_h = false
	elif direction < 0:
		Sprite.flip_h = true
		
func _Jump():
	if Input.is_action_just_pressed("Jump") and (ray_cast_left.is_colliding() or ray_cast_right.is_colliding()) :
		apply_impulse(Vector2(0,-Jump_Force*100))
	
