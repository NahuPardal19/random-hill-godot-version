extends CharacterBody2D

class_name Player

#region VARIABLES
@onready var Sprite = $Sprite
@onready var CanPlay:bool = true
@onready var gun = $Gun

#SCORE
var score : float

#NUMBER PLAYER
enum Player_Number_Enum {PLAYER_1, PLAYER_2 , PLAYER_3 , PLAYER_4}
@export var Player_Number : Player_Number_Enum
var stringPlayerInput : String

#JUMP
var JumpCount

#TIMERS
@onready var jump_buffer_timer = $Timers/JumpBufferTimer
@onready var coyote_jump_timer = $Timers/CoyoteJumpTimer
@onready var respawn_timer = $Timers/RespawnTimer


#RAYCAST
@onready var ray_cast_left = $Raycasts/RayCastLeft
@onready var ray_cast_right = $Raycasts/RayCastRight

#COLOR
@export var Player_Color : Color

#LIGHTS
@onready var light = $PointLight2D

#CAMERA
@export var PlayCamera : PhantomCamera2D

#EVENTS ATRIBUTTES
var Justice_Speed : float

var knockback = Vector2.ZERO
#endregion

#region METODOS

#Ready & Physic process
func _ready():
	InicializarJugador()
func _physics_process(delta):
	_Jump(delta)
	_Move()
	_Gravity()
	_PushOffLedges()
#Movement methods
func _Gravity():
	velocity.y += GAME_MANAGER.Player_Gravity
func _Move():
	var direction = Input.get_axis(stringPlayerInput+"Left",stringPlayerInput+"Right")

	velocity.x = direction * (GAME_MANAGER.Player_Velocity + Justice_Speed) + knockback.x
	knockback = lerp(knockback, Vector2.ZERO, 0.1)

	if direction > 0:
		Sprite.flip_h = false
		gun.RotateGun(false)
	elif direction < 0:
		Sprite.flip_h = true
		gun.RotateGun(true)

		
	var was_on_floor  = is_on_floor()
	move_and_slide()
	var just_left_ledge = was_on_floor and not is_on_floor()
	if just_left_ledge:
		coyote_jump_timer.start()
func _Jump(delta):
	if is_on_floor():
		JumpCount = 1
	if Input.is_action_just_pressed(stringPlayerInput+"Jump"):
		jump_buffer_timer.start()
	if (is_on_floor() and jump_buffer_timer.time_left > 0) or (coyote_jump_timer.time_left > 0.0 and Input.is_action_just_pressed(stringPlayerInput+"Jump")) and JumpCount > 0:
		velocity.y = GAME_MANAGER.Player_JumpForce
		JumpCount -= 1
func _PushOffLedges():
		if ray_cast_left.is_colliding() and !ray_cast_right.is_colliding():
			position.x += 5
		elif !ray_cast_left.is_colliding() and ray_cast_right.is_colliding():
			position.x -= 5
#Others
func InicializarJugador():
	GAME_MANAGER.PlayersList.append(get_node("."))
	
	if Player_Number == Player_Number_Enum.PLAYER_1:
		set_collision_layer_value(1,true)
		stringPlayerInput = "Player_1_"
		Sprite.texture = preload("res://Sprites/Players/Player_1.png")
	elif Player_Number == Player_Number_Enum.PLAYER_2:
		set_collision_layer_value(2,true)
		stringPlayerInput = "Player_2_"
		Sprite.texture = preload("res://Sprites/Players/Player_2.png")
	elif Player_Number == Player_Number_Enum.PLAYER_3:
		set_collision_layer_value(3,true)
		stringPlayerInput = "Player_3_"
		Sprite.texture = preload("res://Sprites/Players/Player_3.png")
	elif Player_Number == Player_Number_Enum.PLAYER_4:
		set_collision_layer_value(4,true)
		stringPlayerInput = "Player_4_"
		Sprite.texture = preload("res://Sprites/Players/Player_4.png")
		
func AddPoints(points : float):
	score += points
	if score < 0: score = 0
	UI_MANAGER.ChangeScore(score,get_node("."))

func PutInvulneravility(time:float):
	Sprite.modulate.a = 0.3
	if Player_Number == Player_Number_Enum.PLAYER_1:
		set_collision_layer_value(1,false)
		set_collision_layer_value(8,true)
	elif Player_Number == Player_Number_Enum.PLAYER_2:
		set_collision_layer_value(2,false)
		set_collision_layer_value(8,true)
	elif Player_Number == Player_Number_Enum.PLAYER_3:
		set_collision_layer_value(3,false)
		set_collision_layer_value(8,true)
	elif Player_Number == Player_Number_Enum.PLAYER_4:
		set_collision_layer_value(4,false)
		set_collision_layer_value(8,true)
	respawn_timer.wait_time = time
	respawn_timer.start()
func QuitInvulnerability():
	Sprite.modulate.a = 1
	if Player_Number == Player_Number_Enum.PLAYER_1:
		set_collision_layer_value(1,true)
		set_collision_layer_value(8,false)
	elif Player_Number == Player_Number_Enum.PLAYER_2:
		set_collision_layer_value(2,true)
		set_collision_layer_value(8,false)
	elif Player_Number == Player_Number_Enum.PLAYER_3:
		set_collision_layer_value(3,true)
		set_collision_layer_value(8,false)
	elif Player_Number == Player_Number_Enum.PLAYER_4:
		set_collision_layer_value(4,true)
		set_collision_layer_value(8,false)
func _on_respawn_timer_timeout():
	QuitInvulnerability()
