extends Node2D

@onready var game_timer = $Game_Timer
@onready var event_timer = $Event_Timer
@onready var beetween_events_timer = $BeetweenEvents_Timer

@export var PhantomMainCamera : PhantomCamera2D
@export var MainCamera : Camera2D
@export var EventContainer:Node2D

var events = []

func _ready():
	_InicializacionPartida()
	GAME_MANAGER.IniciarPartida()

func _process(delta):
	pass

func _InicializacionPartida():
	Engine.time_scale = 1
	GAME_MANAGER.Spawners.clear()
	
	_AgregarEventos()
	GAME_MANAGER.TimerPlay = game_timer
	GAME_MANAGER.TimerEvent = event_timer
	GAME_MANAGER.TimerBeetweenEvents = beetween_events_timer
	GAME_MANAGER.Events = self.events
	GAME_MANAGER.MainCamera = self.MainCamera
	GAME_MANAGER.PhantomGeneralCamera = PhantomMainCamera

func _AgregarEventos():
	events.clear()
	
	for i in EventContainer.get_child_count():
		if EventContainer.get_child(i).visible:
			events.append(EventContainer.get_child(i))
		

func _on_game_time_timeout():
	GAME_MANAGER.FinalizarPartida()
func _on_event_timer_timeout():
	GAME_MANAGER.FinalizarEvento()
func _on_beetween_events_timer_timeout():
	GAME_MANAGER.IniciarEvento()
