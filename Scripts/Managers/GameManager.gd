extends Node

#TODO ///// GAME CONFIGURATION

#GENERAL PLAYER STATS
var Player_Velocity : float = 200.0
var Player_JumpForce : float = -350
var Player_Gravity : float = 20.0
var Player_KnockbackForce : float = 1000.0
var Player_RespawnTime : float = 5.0
var Deadzone = 0.3

#GUN STATS
var NormalGun_ForceShoot:float = 400.0

#GAME STATS
var PlayersToPlay = []
var PlayersList = []

var Score_To_Win : int = 80

var TimePlay : float = 180.0
var TimerPlay : Timer

var TimerEvent : Timer
var TimePerEvent : float = 30.0

var TimerBeetweenEvents : Timer
var TimeBeetweenEvents : float = 10.0

#VARIABLES DE CONTROL
var OnGame : bool
var OnSuddenDeath : bool
var rnd = RandomNumberGenerator.new()

#EVENTS
var Events = []
var event_selected : Evento

#CAMERA
var PhantomGeneralCamera : PhantomCamera2D
var MainCamera : Camera2D

#ARRAYS
var Spawners = []

#SIGNALS
signal InicioEvento(EventName : String)
signal FinalEvento(EventName : String)
signal InicioPartida
signal FinalPartida(Winner:Player)
signal InicioMuerteSubita

func _ready():
	_SetearDeadZone()

	
func _process(delta):
	if OnGame:
		UI_MANAGER.ChangeTime(TimerPlay.time_left)
		_TestWinOnGame()
	
func _SetearDeadZone():
	
	InputMap.action_set_deadzone("Player_2_AimDown",Deadzone)
	InputMap.action_set_deadzone("Player_2_AimUp",Deadzone)
	InputMap.action_set_deadzone("Player_2_AimLeft",Deadzone)
	InputMap.action_set_deadzone("Player_2_AimRight",Deadzone)
	
	InputMap.action_set_deadzone("Player_3_AimDown",Deadzone)
	InputMap.action_set_deadzone("Player_3_AimUp",Deadzone)
	InputMap.action_set_deadzone("Player_3_AimLeft",Deadzone)
	InputMap.action_set_deadzone("Player_3_AimRight",Deadzone)
	
	InputMap.action_set_deadzone("Player_4_AimDown",Deadzone)
	InputMap.action_set_deadzone("Player_4_AimUp",Deadzone)
	InputMap.action_set_deadzone("Player_4_AimLeft",Deadzone)
	InputMap.action_set_deadzone("Player_4_AimRight",Deadzone)
	
func _ConseguirSpawnerRandom() -> Spawner:
	var spawner : Spawner =  Spawners[rnd.randi_range(0,Spawners.size()-1)]
	if spawner.get_parent().process_mode != PROCESS_MODE_DISABLED && spawner.playersOnSpawn.size() < 1:
		return spawner
	else: return _ConseguirSpawnerRandom()

#region Public methods
func IniciarPartida():
	TimerPlay.wait_time = TimePlay
	TimerPlay.start()
	IniciarBeetweenEvents()
	InicioPartida.emit()
	#PONGO LOS JUGADORES DEL LOBBY
	
	for i in range(PlayersList.size()-1,-1,-1):
		if PlayersList[i] == null: PlayersList.remove_at(i)
	for i in range(Spawners.size()-1,-1,-1):
		if Spawners[i] == null: Spawners.remove_at(i)
	
	for n in 4:
		PlayersList[n].visible = PlayersToPlay[n]
			
	
	for n in range(PlayersList.size()-1,-1,-1):
		if 	!PlayersList[n].visible:
			PlayersList[n].queue_free()
			PlayersList.remove_at(n)
			if n == 0:
				UI_MANAGER._ScorePlayer1Label.hide()
			if n == 1:
				UI_MANAGER._ScorePlayer2Label.hide()
			if n == 2:
				UI_MANAGER._ScorePlayer3Label.hide()
			if n == 3:
				UI_MANAGER._ScorePlayer4Label.hide()
			
	OnSuddenDeath = false
	OnGame = true
func FinalizarPartida():
	_TestWinForTimerOut()

func IniciarEvento():

	TimerEvent.wait_time = TimePerEvent
	TimerEvent.start()
	
	rnd.randomize()
	event_selected = Events[rnd.randi_range(0,Events.size()-1)]
	event_selected.InitEvent()
	InicioEvento.emit(event_selected.EventName)
func FinalizarEvento():
	event_selected.FinishEvent()
	IniciarBeetweenEvents()
	
	FinalEvento.emit("")
	
func IniciarBeetweenEvents():
	TimerBeetweenEvents.wait_time = TimeBeetweenEvents
	TimerBeetweenEvents.start()

func IniciarMuerteSubita(DefeatPlayers : Array):
	for player in DefeatPlayers:
		PlayersList.remove_at(PlayersList.find(player))
		player.queue_free()
	
	OnSuddenDeath = true
	InicioMuerteSubita.emit()

func _TestWinOnGame():
	if OnGame:
		for player in PlayersList:
			if player.score >= Score_To_Win:
				_EndGame(player)
	if OnSuddenDeath:
		if PlayersList.size() == 1:
			_EndGame(PlayersList[0])
func _TestWinForTimerOut():
	var PlayersWithMorePoints = []
	var DefeatPlayers = []
	for player in PlayersList:
		if PlayersWithMorePoints.size() <= 0:
			PlayersWithMorePoints.clear()
			PlayersWithMorePoints.append(player)
		else:
			if round(player.score) > round(PlayersWithMorePoints[0].score):
				PlayersWithMorePoints.clear()
				PlayersWithMorePoints.append(player)
			elif round(player.score) == round(PlayersWithMorePoints[0].score):
				PlayersWithMorePoints.append(player)
			else: DefeatPlayers.append(player)
				
	if PlayersWithMorePoints.size() == 1:
		_EndGame(PlayersWithMorePoints[0])
	else:
		IniciarMuerteSubita(DefeatPlayers)
func _EndGame(winnerP):
	UI_MANAGER.PutGameOverUI(true)
	var winner : Player = winnerP
	Engine.time_scale = 0
	FinalPartida.emit(winner)
	OnGame = false

func SpawnearJugador(player):
	print(player.get_name())
	rnd.randomize()
	player.global_position = _ConseguirSpawnerRandom().global_position + Vector2.UP * 5
	player.velocity = Vector2.ZERO
	player.PutInvulneravility(Player_RespawnTime)
#endregion
