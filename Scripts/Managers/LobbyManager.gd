extends CanvasLayer


#PLAYERS UI

@onready var player_1_ready = $CanvasLayer/HBoxContainer/Fondo/Player1_Ready
@onready var player_1_text = $CanvasLayer/HBoxContainer/Fondo/Player1Text

@onready var player_2_ready = $CanvasLayer/HBoxContainer/Fondo2/Player2_Ready
@onready var player_2_text = $CanvasLayer/HBoxContainer/Fondo2/Player2Text

@onready var player_3_ready = $CanvasLayer/HBoxContainer/Fondo3/Player3_Ready
@onready var player_3_text = $CanvasLayer/HBoxContainer/Fondo3/Player3Text

@onready var player_4_ready = $CanvasLayer/HBoxContainer/Fondo4/Player4_Ready
@onready var player_4_text = $CanvasLayer/HBoxContainer/Fondo4/Player4Text

var cantidadPlayer : int 
var CanPlay : bool

var PlayersArray = []

func _ready():
	PlayersArray.append(false)
	PlayersArray.append(false)
	PlayersArray.append(false)
	PlayersArray.append(false)
				
func _process(delta):
	_ReadyPlayer()
	
func _ReadyPlayer():
	if Input.is_action_just_pressed("Player_1_Jump"):
		if !PlayersArray[0]:
			player_1_ready.show()
			player_1_text.hide()
			PlayersArray[0] = true
			cantidadPlayer+=1
			_ActualizarCantidadJugadores()
		
	elif Input.is_action_just_pressed("Player_2_Jump"):
		if !PlayersArray[1]:
			player_2_ready.show()
			player_2_text.hide()
			PlayersArray[1] = true
			cantidadPlayer+=1
			_ActualizarCantidadJugadores()
		
	elif Input.is_action_just_pressed("Player_3_Jump"):
		if !PlayersArray[2]:
			player_3_ready.show()
			player_3_text.hide()
			PlayersArray[2] = true
			cantidadPlayer+=1
			_ActualizarCantidadJugadores()
		
	elif Input.is_action_just_pressed("Player_4_Jump"):
		if !PlayersArray[3]:
			player_4_ready.hide()
			player_4_text.show()
			PlayersArray[3] = true
			cantidadPlayer+=1
			_ActualizarCantidadJugadores()
		
	if Input.is_action_just_pressed("Player_1_Quit"):
		if PlayersArray[0]:
			player_1_ready.hide()
			player_1_text.show()
			PlayersArray[0] = false
			cantidadPlayer-=1
			_ActualizarCantidadJugadores()
		
	elif Input.is_action_just_pressed("Player_2_Quit"):
		if PlayersArray[1]:
			player_2_ready.hide()
			player_2_text.show()
			PlayersArray[1] = false
			cantidadPlayer-=1
			_ActualizarCantidadJugadores()
		
	elif Input.is_action_just_pressed("Player_3_Quit"):
		if PlayersArray[2]:
			player_3_ready.hide()
			player_3_text.show()
			PlayersArray[2] = false
			cantidadPlayer-=1
			_ActualizarCantidadJugadores()
		
	elif Input.is_action_just_pressed("Player_4_Quit"):
		if PlayersArray[3]: 
			player_4_ready.hide()
			player_4_text.show()
			PlayersArray[3] = false
			cantidadPlayer-=1
			_ActualizarCantidadJugadores()

func _on_button_pressed():
	if CanPlay:
		GAME_MANAGER.PlayersToPlay = PlayersArray
		get_tree().change_scene_to_file("res://Scenes/Maps/Test map.tscn")
	
func _ActualizarCantidadJugadores():
	if cantidadPlayer < 2: 
		$CanvasLayer/Label.modulate = Color.RED
		CanPlay = false
	else:
		$CanvasLayer/Label.modulate = Color.WHITE
		CanPlay = true
	$CanvasLayer/Label.text = str(cantidadPlayer) + "/4"
	
