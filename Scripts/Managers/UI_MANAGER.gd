extends Node

#TODO UI MANAGER

var _ScorePlayer1Label
var _ScorePlayer2Label 
var _ScorePlayer3Label 
var _ScorePlayer4Label 


var _TimerLabel : Label

var _EventLabel : Label

var _GameOverUI 

func _ready():
	GAME_MANAGER.InicioEvento.connect(_ChangeEventText)
	GAME_MANAGER.FinalEvento.connect(_ChangeEventText)
	GAME_MANAGER.InicioMuerteSubita.connect(_SuddenDeathUI)

func _process(delta):
	pass

func ChangeScore(score:float , player:Player):
	
	var ScoreRounded = round(score)
	
	if player.Player_Number == Player.Player_Number_Enum.PLAYER_1:
		_ScorePlayer1Label.get_child(1).text = str(ScoreRounded)
	if player.Player_Number == Player.Player_Number_Enum.PLAYER_2:
		_ScorePlayer2Label.get_child(1).text = str(ScoreRounded)
	if player.Player_Number == Player.Player_Number_Enum.PLAYER_3:
		_ScorePlayer3Label.get_child(1).text = str(ScoreRounded)
	if player.Player_Number == Player.Player_Number_Enum.PLAYER_4:
		_ScorePlayer4Label.get_child(1).text = str(ScoreRounded)	
func ChangeTime(time_left : float):
	_TimerLabel.text = str(snappedf(time_left,1))
func PutGameOverUI(IsVisible : bool):
	_GameOverUI.visible = IsVisible
	
func _ChangeEventText(eventName : String):
	_EventLabel.text = eventName
func _SuddenDeathUI():
	_TimerLabel.text = "SUDDEN DEATH"
