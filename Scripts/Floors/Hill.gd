extends StaticBody2D

static var PlayersOnHill = []
static var InitialColor : Color

func _ready():
	InitialColor = $Sprite2D.modulate

func _process(delta):
	_AddPointsForPlayer(delta)
	
func _on_area_hill_body_entered(body):
	if body.is_in_group("Player"):
		PlayersOnHill.append(body)
		
func _on_area_hill_body_exited(body):
	if body.is_in_group("Player"):
		PlayersOnHill.remove_at(PlayersOnHill.find(body))
		
func _AddPointsForPlayer(delta):
	if PlayersOnHill.size() == 1:
		var p : Player = PlayersOnHill[0]
		p.AddPoints(1*delta)
		$Sprite2D.modulate = p.Player_Color
	else:
		$Sprite2D.modulate = InitialColor
