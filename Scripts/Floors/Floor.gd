extends StaticBody2D

@onready var animation_player = $AnimationPlayer

var color : Color 

func _ready():
	EventFloors.AllFloors.append(get_node("."))
	color = $Area2D/Sprite2D.modulate
	
func SetFloor(active : bool):
	if active:
		$CollisionShape2D.disabled = false
		$Area2D/CollisionShape2D.disabled = false
		$Area2D/Sprite2D.visible = true
		$Area2D/Sprite2D.modulate = color
	else:
		animation_player.queue("Destroy Floor")
