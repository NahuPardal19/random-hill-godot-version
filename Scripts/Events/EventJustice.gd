extends Evento

func InitEvent():
	OnEvent = true
	GAME_MANAGER.PlayersList.sort_custom(_sort_by_points)
	_ApplyJustice()

func FinishEvent():
	OnEvent = false
	_DesapplyJustice()

func _init():
	OnEvent = false
	EventName = "Justice" 

func _sort_by_points(a,b) -> bool:
	if a.score > b.score:
		return true
	return false
func _ApplyJustice():
	var playerAux : Player
	for player in GAME_MANAGER.PlayersList:
		if playerAux != null:
			if round(player.score) == round(playerAux.score):
				player.Justice_Speed = playerAux.Justice_Speed
			else:
				player.Justice_Speed = playerAux.Justice_Speed + 50.0
		else:
			player.Justice_Speed = -150.0
		playerAux = player
		print(player.get_name() + ": " + str(player.Justice_Speed))
func _DesapplyJustice():
	for player in GAME_MANAGER.PlayersList:
		player.Justice_Speed = 0
