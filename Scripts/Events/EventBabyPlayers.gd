extends Evento

func _init():
	OnEvent = false
	EventName = "baby players"

func InitEvent():
	OnEvent = true
	for player in GAME_MANAGER.PlayersList:
		player.scale = Vector2(0.6,0.6)
func FinishEvent():
	OnEvent = false
	for player in GAME_MANAGER.PlayersList:
		player.scale = Vector2(1.0,1.0)


