extends Node2D

class_name Evento

var OnEvent : bool
var EventName : String

func InitEvent():
	OnEvent = true

func FinishEvent():
	OnEvent = false

func _init():
	OnEvent = false
