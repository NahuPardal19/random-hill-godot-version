extends Evento

@export var DirectionalLight : DirectionalLight2D

func InitEvent():
	OnEvent = true
	DirectionalLight.show()
	for player in GAME_MANAGER.PlayersList:
		var playerAux : Player = player
		playerAux.light.show()

func FinishEvent():
	OnEvent = false
	DirectionalLight.hide()
	for player in GAME_MANAGER.PlayersList:
		var playerAux : Player = player
		playerAux.light.hide()

func _init():
	OnEvent = false
	EventName = "Lights Off"

