extends Evento

var rnd = RandomNumberGenerator.new()

func InitEvent():
	OnEvent = true
	_ZoomearPlayer()

func FinishEvent():
	OnEvent = false
	_DeszoomearPlayers()

func _init():
	OnEvent = false
	EventName = "ZOOM THE WORST"

func _ZoomearPlayer():
	var PlayersWithLeastPoints = []
	for player in GAME_MANAGER.PlayersList:
		if PlayersWithLeastPoints.size() <= 0:
			PlayersWithLeastPoints.clear()
			PlayersWithLeastPoints.append(player)
		else:
			if round(player.score) < round(PlayersWithLeastPoints[0].score):
				PlayersWithLeastPoints.clear()
				PlayersWithLeastPoints.append(player)
			elif round(player.score) == round(PlayersWithLeastPoints[0].score):
				PlayersWithLeastPoints.append(player)
	if PlayersWithLeastPoints.size() == 1:
		var cameraPlayer : PhantomCamera2D = PlayersWithLeastPoints[0].PlayCamera
		cameraPlayer.set_priority(10)
	else:
		rnd.randomize()
		var playerSelectedToZoom : Player = PlayersWithLeastPoints[rnd.randi_range(0,PlayersWithLeastPoints.size()-1)]
		var cameraPlayer : PhantomCamera2D = playerSelectedToZoom.PlayCamera
		cameraPlayer.set_priority(11)
func _DeszoomearPlayers():
	for player in GAME_MANAGER.PlayersList:
		var playerAux : Player = player
		playerAux.PlayCamera.set_priority(0)
	GAME_MANAGER.MainCamera.set_priority(10)
	GAME_MANAGER.MainCamera.MainCamera.set_zoom(Vector2(6,6))
