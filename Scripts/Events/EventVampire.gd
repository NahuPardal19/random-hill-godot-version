extends Evento

class_name EventVampire

static var vampireIsActive : bool
static var PointsToSteal : float

func InitEvent():
	OnEvent = true
	vampireIsActive = true
	for player in SeleccionarPlayersHill():
		pass

func FinishEvent():
	OnEvent = false
	vampireIsActive = false

func _init():
	OnEvent = false
	vampireIsActive = false
	EventName = "Vampire" 
	PointsToSteal = 10.0

func SeleccionarPlayersHill() -> Array:
	var PlayersWithMostPoints = []
	for player in GAME_MANAGER.PlayersList:
		if PlayersWithMostPoints.size() <= 0:
			PlayersWithMostPoints.clear()
			PlayersWithMostPoints.append(player)
		else:
			if round(player.score) > round(PlayersWithMostPoints[0].score):
				PlayersWithMostPoints.clear()
				PlayersWithMostPoints.append(player)
			elif round(player.score) == round(PlayersWithMostPoints[0].score):
					PlayersWithMostPoints.append(player)
	return PlayersWithMostPoints
