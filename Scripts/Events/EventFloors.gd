extends Evento

class_name EventFloors

static var AllFloors = []
var rnd = RandomNumberGenerator.new()
@onready var timer = $Timer

@export var timePerDestroys:float
@export var FloorsToDestroy:int

func InitEvent():
	OnEvent = true
	timer.wait_time = timePerDestroys
	timer.start()

func FinishEvent():
	OnEvent = false
	for floor in AllFloors:
		floor.SetFloor(true)

func _init():
	AllFloors.clear()
	print(AllFloors.size())
	#for i in range(AllFloors.size()-1,-1,-1):
		#if AllFloors[i] == null: AllFloors.remove_at(i)
	OnEvent = false
	EventName = "Bye Floors" 
	

func _DestroyFloors():
	if OnEvent:
		for n in FloorsToDestroy:
			var SelectedFloor
			rnd.randomize()
			SelectedFloor = AllFloors[rnd.randi_range(0,AllFloors.size()-1)]
			if SelectedFloor.visible:
				SelectedFloor.SetFloor(false)


func _on_timer_timeout():
	_DestroyFloors()
	timer.wait_time = timePerDestroys
	timer.start()
