extends Evento

class_name Event_Superplayer

var Player_Velocity_NORMAL : float 
var Player_JumpForce_NORMAL : float 

func _init():
	Player_Velocity_NORMAL = GAME_MANAGER.Player_Velocity
	Player_JumpForce_NORMAL = GAME_MANAGER.Player_JumpForce
	EventName = "SUPER PLAYERS"

func InitEvent():
	OnEvent = true
	GAME_MANAGER.Player_Velocity = 400.0
	GAME_MANAGER.Player_JumpForce = -500.0

func FinishEvent():
	OnEvent = false
	GAME_MANAGER.Player_Velocity = Player_Velocity_NORMAL
	GAME_MANAGER.Player_JumpForce = Player_JumpForce_NORMAL

