extends Evento

@export var camera : PhantomCamera2D
var zoomInvertido:Vector2

func _init():
	OnEvent = false
	EventName = "Inverse"

func InitEvent():
	OnEvent = true
	camera.rotation_degrees = 180.0

func FinishEvent():
	OnEvent = false
	camera.rotation_degrees = 0
