extends Evento

class_name Event_Gravity

func _init():
	OnEvent = false
	EventName = "Gravity off"

func _process(delta):
	#print(OnEvent)
	if OnEvent:
		GAME_MANAGER.Player_Gravity = 10.0
	else:
		GAME_MANAGER.Player_Gravity = 20.0
