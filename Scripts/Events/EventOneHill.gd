extends Evento

class_name Event_OneHill

@export var MapaNormal : TileMap
@export var MapaEvento: TileMap

func _init():
	OnEvent = false
	EventName = "One Floor"

func _process(delta):
	pass

func InitEvent():
	OnEvent = true
	
	MapaEvento.process_mode = Node.PROCESS_MODE_INHERIT
	MapaEvento.visible = true
	MapaNormal.process_mode = Node.PROCESS_MODE_DISABLED
	MapaNormal.visible = false
	
	print(GAME_MANAGER.Spawners.size())
	
	for player in GAME_MANAGER.PlayersList:
		GAME_MANAGER.SpawnearJugador(player)
	
	print(GAME_MANAGER.Spawners.size())
	
func FinishEvent():
	
	OnEvent = false
	
	MapaEvento.process_mode = Node.PROCESS_MODE_DISABLED
	MapaEvento.visible = false
	MapaNormal.process_mode = Node.PROCESS_MODE_INHERIT
	MapaNormal.visible = true
	
	
	for player in GAME_MANAGER.PlayersList:
		GAME_MANAGER.SpawnearJugador(player)
