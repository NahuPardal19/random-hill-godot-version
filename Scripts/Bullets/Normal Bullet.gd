extends RigidBody2D

var player : Player

func _on_area_2d_area_entered(area):
	if area.get_name() == "Area2D":
		queue_free()

func _on_visible_on_screen_notifier_2d_screen_exited():
	queue_free()

func _on_area_2d_body_entered(body):
	if body.is_in_group("Player"):
		if body.Player_Number != player.Player_Number:
			var direction = global_position.direction_to(body.global_position)
			var bullet_force = direction * GAME_MANAGER.Player_KnockbackForce
			body.knockback = bullet_force
			if EventVampire.vampireIsActive:
				if body.score >= 10:
					player.AddPoints(EventVampire.PointsToSteal)
				else:
					player.AddPoints(body.score)
				body.AddPoints(-EventVampire.PointsToSteal)
			queue_free()
